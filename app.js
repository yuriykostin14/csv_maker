var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');

var globalContentArray = [];
var globalTempArray = [];
var globalName = [];
var globalLevel = [];
var globalTeamID = 0;
var globalNumArray = [];
var globalLength = [];

var csvWriter = require('csv-write-stream');
                    
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public'))); //  "public" off of current is root
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/',function(req,res){
	res.render('index');
});

function indexOfSpace(str) {
    return str.indexOf(' ');
}

function indexOfSlash(str) {
    return str.indexOf('/');
}

function indexOfDash(str) {
    return str.indexOf('-');
}

function indexOfCommaSpace(str) {
    return str.indexOf(', ');
}

function indexOfLastDot(str) {
    return str.lastIndexOf('.');
}

function getJersey(str) {
    try{
        return str.match(/\d+/)[0];
    }
    catch(err){
        return 'Jersey #';
    }
}

function getName(str, index) {
    return str.substring(0, index);
}

function getRestString(str, str1) {
    var n = str1.localeCompare("RS");
    var result = [];
    if (n == 0) {
        var temp = str.replace(str1 + " ", "");
        var other = getName(temp, indexOfSpace(temp));
        result[0] = other;
        result[1] = temp.replace(other + " ",  "");
    }
    else {
        result[0] = "";
        result[1] = str.replace(str1 + " ", "")
    }
    return result;
}

function getPos2(str, str1) {
    return str.replace(str1 + "/", "");
}

function getHtInches(str, str1) {
    return str.replace(str1 + "-", "");
}

function getRestHome(str, str1) {
    return str.replace(str1 + ", ", "");
}

app.post('/next', function (req, res) {
    var content = req.body.content;
    globalName.push(req.body.name);
    globalLevel.push(req.body.level);
    globalTeamID++;

    var eachCon = "";
    var jersey = "";
    var firstName = "";
    var lastName = "";
    var pos = "";
    var bt = "";
    var cl = "";
    var ht = "";
    var wt = "";
    var home = "";

    content = content.replace(/\t/gi, " ");

    var regex = /\r|\n/gi, result, indexes = [];
    indexes.push(0);
    var tempIndex = 0;
    while ( (result = regex.exec(content)) ) {
        tempIndex++;
        if (tempIndex % 2 == 1){
            indexes.push(result.index);
        }
    }
 
    var contentArray = [];
    var tempArray = [];
    var flag = [];
    for (var i = 0; i < indexes.length; i ++) {
        eachCon = content.substring(indexes[i], indexes[i+1]);
        eachCon = eachCon.replace('\r\n', '');
        eachCon = eachCon.replace("' ", "-");
        eachCon = eachCon.replace("'", "-");
        eachCon = eachCon.replace("/1", " 1");
        eachCon = eachCon.replace("/2", " 2");
        eachCon = eachCon.replace("/ 1", "1");
        eachCon = eachCon.replace("/ 2", "2");
        eachCon = eachCon.replace('"', "");

        jersey = getJersey(eachCon);

        flag = getRestString(eachCon, jersey);
        eachCon = flag[1];
        firstName = getName(eachCon, indexOfSpace(eachCon));
        
        flag = getRestString(eachCon, firstName);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            firstName += " " + flag[0];
        }
        lastName = getName(eachCon, indexOfSpace(eachCon));

        flag = getRestString(eachCon, lastName);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            lastName += " " + flag[0];
        }
        pos = getName(eachCon, indexOfSpace(eachCon));
        
        flag = getRestString(eachCon, pos);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            pos += " " + flag[0];
        }
        bt = getName(eachCon, indexOfSpace(eachCon));
        
        flag = getRestString(eachCon, bt);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            bt += " " + flag[0];
        }
        cl = getName(eachCon, indexOfSpace(eachCon));

        flag = getRestString(eachCon, cl);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            cl += " " + flag[0];
        }
        ht = getName(eachCon, indexOfSpace(eachCon));

        flag = getRestString(eachCon, ht);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            ht += " " + flag[0];
        }
        wt = getName(eachCon, indexOfSpace(eachCon));

        flag = getRestString(eachCon, wt);
        var n = flag[0].localeCompare("");
        eachCon = flag[1];
        if (n != 0) {
            wt += " " + flag[0];
        }
        home = eachCon;

        firstName = firstName.replace(",", "");
        
        if (i != 0) {
            tempArray[tempArray.length] = {
                firstName: firstName,
                lastName: lastName,
                jersey: jersey,
                pos: pos,
                bt: bt,
                cl: cl,
                ht: ht,
                wt: wt,
                home: home,
                teamID: globalTeamID
            };
        }   

        contentArray[contentArray.length] = {
            firstName: firstName,
            lastName: lastName,
            jersey: jersey,
            pos1: pos,
            bt: bt,
            cl: cl,
            ht: ht,
            wt: wt,
            city: home,
            teamID: globalTeamID
        };
    }

    globalContentArray.push.apply(globalContentArray, contentArray);

    res.render('generate', {
        name: req.body.name,
        level: req.body.level,
        content: tempArray,
        length: tempArray.length
    });
});

app.post('/generateFlightscopeCSV', function (req, res){
    var params = req.body.params;
    var numArray = [params.indexOf('jersey'), params.indexOf('firstName'), params.indexOf('lastName')];

    globalLength.push(params[9]);
    globalNumArray.push.apply(globalNumArray, numArray);


    var writer = csvWriter();
    var writer = csvWriter({ headers: ["Team ID", "Team", "Class", "Player ID", "First Name", "Last Name", "#", "Position", "Grad Year", "Bats", "Throws", "Height", "Weight", "City", "State", "Country"]})
    writer.pipe(fs.createWriteStream('public/Flightscope.csv'));
    var index = 1;
    var i = 0;

    globalTempArray = [];
    var temp0  = [], temp1 = [], temp2 = [], temp3 = [], temp4 = [], temp5 = [], temp6 = [], temp7 = [], temp8 = [] ;

    for (i = 0; i < globalContentArray.length; i++) {
        temp0.push(globalContentArray[i].jersey);
        temp1.push(globalContentArray[i].firstName);
        temp2.push(globalContentArray[i].lastName);
        temp3.push(globalContentArray[i].pos1);
        temp4.push(globalContentArray[i].cl);
        temp5.push(globalContentArray[i].ht);
        temp6.push(globalContentArray[i].wt);
        temp7.push(globalContentArray[i].city);
        temp8.push(globalContentArray[i].bt);
    } 

    globalTempArray[0] = temp0;
    globalTempArray[1] = temp1;
    globalTempArray[2] = temp2;
    globalTempArray[3] = temp3;
    globalTempArray[4] = temp4;
    globalTempArray[5] = temp5;
    globalTempArray[6] = temp6;
    globalTempArray[7] = temp7;
    globalTempArray[8] = temp8;

    var length = 0;
    var interval = 0;
    var cmpLength = 0;
    cmpLength = globalLength[0];

    for (i = 0; i < globalContentArray.length; i++){
        if (globalContentArray[i].jersey == 'Jersey #')
            continue;
        writer.write([globalContentArray[i].teamID, globalName[globalContentArray[i].teamID  - 1], '', index, globalTempArray[globalNumArray[1 + length]][i], globalTempArray[globalNumArray[2 + length]][i], 
                    globalTempArray[globalNumArray[0 + length]][i], '', '', '', '', '', '', '', '', '']);    
        if (index == cmpLength) {
            length += 8;
            interval++;
            cmpLength = 0;
            for (var j = 0; j <= interval; j++){
                cmpLength = parseInt(cmpLength) + parseInt(globalLength[j]);
            }
        }
        index++;
    }

    writer.end();
    
    globalContentArray = [];
    globalTempArray = [];
    globalName = [];
    globalLevel = [];
    globalTeamID = 0;
    globalNumArray = [];
    globalLength = [];

    res.send(globalContentArray);
});

app.post('/addAnotherTeamToCSV', function (req, res) {
    if (globalContentArray.length == 0 && globalTempArray.length == 0 && globalName.length == 0 && globalLevel.length == 0 && globalTeamID == 0 && globalNumArray.length == 0 && globalLength.length == 0) {
        res.status(200).json({ message: 'ok' });
        return;
    }
    var params = req.body.params;

    globalLength.push(params[9]);
    var numArray = [params.indexOf('jersey'), params.indexOf('firstName'), params.indexOf('lastName'), params.indexOf('pos'), 
        params.indexOf('cl'), params.indexOf('ht'), params.indexOf('wt'), params.indexOf('home')];

    globalNumArray.push.apply(globalNumArray, numArray);

    res.status(200).json({ message: 'ok' });
});

app.post('/generateDatabaseCSV', function (req, res){
    var params = req.body.params;
    var numArray = [params.indexOf('jersey'), params.indexOf('firstName'), params.indexOf('lastName'), params.indexOf('pos'), 
        params.indexOf('cl'), params.indexOf('ht'), params.indexOf('wt'), params.indexOf('home')];

    globalLength.push(params[9]);
    globalNumArray.push.apply(globalNumArray, numArray);


    var writer = csvWriter();
    var writer = csvWriter({ headers: ["id", "jersey", "first", "last", "pos1", "pos2", "cl", "ht", "ht_inches", "wt", "city", "state", "level", "school"]});
    writer.pipe(fs.createWriteStream('public/Database.csv'));
    var index = 1;
    var i = 0;

    globalTempArray = [];
    var temp0  = [], temp1 = [], temp2 = [], temp3 = [], temp4 = [], temp5 = [], temp6 = [], temp7 = [], temp8 = [] ;

    for (i = 0; i < globalContentArray.length; i++) {
        temp0.push(globalContentArray[i].jersey);
        temp1.push(globalContentArray[i].firstName);
        temp2.push(globalContentArray[i].lastName);
        temp3.push(globalContentArray[i].pos1);
        temp4.push(globalContentArray[i].cl);
        temp5.push(globalContentArray[i].ht);
        temp6.push(globalContentArray[i].wt);
        temp7.push(globalContentArray[i].city);
        temp8.push(globalContentArray[i].bt);
    } 

    globalTempArray[0] = temp0;
    globalTempArray[1] = temp1;
    globalTempArray[2] = temp2;
    globalTempArray[3] = temp3;
    globalTempArray[4] = temp4;
    globalTempArray[5] = temp5;
    globalTempArray[6] = temp6;
    globalTempArray[7] = temp7;
    globalTempArray[8] = temp8;

    var length = 0;
    var interval = 0;
    var cmpLength = 0;
    cmpLength = globalLength[0];
    var posTemp = "", pos1 = "", pos2 = "";
    var htTemp = "", ht1 = "", ht2 = ""; 
    var homeTemp  = "", city = "", state = "";

    for (i = 0; i < globalContentArray.length; i++){
        if (globalContentArray[i].jersey == 'Jersey #')
            continue;

        posTemp = globalTempArray[globalNumArray[3 + length]][i];
        pos1 = "";
        pos2 = "";
        var slashIndex = indexOfSlash(posTemp);
        if (slashIndex == -1) {
            pos1 = posTemp;
            pos2 = "";
        }
        else {
            pos1 = getName(posTemp, slashIndex);
            pos2 = getPos2(posTemp, pos1);
        }

        htTemp = globalTempArray[globalNumArray[5 + length]][i];
        ht1 = "";
        ht2 = "";
        var slashIndex = indexOfDash(htTemp);
        if (slashIndex == -1) {
            ht1 = htTemp;
            ht2 = "";
        }
        else {
            ht1 = getName(htTemp, slashIndex);
            ht2 = getHtInches(htTemp, ht1);
        }

        var homeTemp = globalTempArray[globalNumArray[7 + length]][i];
        var city = "";
        var state = "";
        var spaceIndex = indexOfCommaSpace(homeTemp);

        city = getName(homeTemp, spaceIndex);
        homeTemp = getRestHome(homeTemp, city);
        state = getName(homeTemp, indexOfSpace(homeTemp));
        var lastDot = indexOfLastDot(state);
        if (lastDot != -1)
            state = getName(state, indexOfLastDot(state));
        
        writer.write([index, globalTempArray[globalNumArray[0 + length]][i], globalTempArray[globalNumArray[1 + length]][i], globalTempArray[globalNumArray[2 + length]][i], pos1, pos2, globalTempArray[globalNumArray[4 + length]][i], 
                    ht1, ht2, globalTempArray[globalNumArray[6 + length]][i], city, state, globalLevel[globalContentArray[i].teamID  - 1], globalName[globalContentArray[i].teamID  - 1]]);
        
        if (index == cmpLength) {
            length += 8;
            interval++;
            cmpLength = 0;
            for (var j = 0; j <= interval; j++){
                cmpLength = parseInt(cmpLength) + parseInt(globalLength[j]);
            }
        }
        index++;
    }
 
    writer.end();
    
    globalContentArray = [];
    globalTempArray = [];
    globalName = [];
    globalLevel = [];
    globalTeamID = 0;
    globalNumArray = [];
    globalLength = [];

    res.send(globalContentArray);
});

app.post('/goBack', function(req, res) {
    var length = parseInt(req.body.length) + 1;

    if (globalContentArray.length == 0 && globalLength.length == 0 && globalName.length == 0 && globalTeamID == 0){
        res.status(200).json({ message: 'cancel' });
        return;
    }

    else if (globalLength.length == 0) {
        globalContentArray.length = globalContentArray.length - length;
        globalName.length = globalName.length - 1;
        globalTeamID--;
        res.status(200).json({ message: 'cancel' });
        return;
    }
    
    globalContentArray.length = globalContentArray.length - length;
    globalLength.length = globalLength - 1;
    globalName.length = globalName.length - 1;
    globalTeamID--;

    res.status(200).json({ message: 'ok' });
});

var port = process.env.PORT || '3000';

app.listen(port, function(){
	console.log('running on ' + port + '...');
});
